<?php 


# Monte uma lógica para retorna os valores das operações aritiméticas. Use seus conhecimentos e sua criatividade, o importante é errar e aprender.
# Facilitamos seu trabalho e já declaramos as variaveis necessariais! Boa sorte!

$valor1 = $_GET["valor1"];
$valor2 = $_GET["valor2"];
$operacao = $_GET["operacao"];

function calcular($op, $var1, $var2){
    
    switch($op){
        case '+':
            return $var1 + $var2;
            break;
            
        case '-':
            return $var1 - $var2;
            break;
            
        case '*':
            return $var1 * $var2;
            break;
            
        case "/":
            return $var1 / $var2;
            break;
            
        default:
            return "<h1>Dados incompletos! <a href='./index.php'>Preencha novamente<a></h1>";
    }
    
}

echo calcular($operacao,$valor1,$valor2);


?>